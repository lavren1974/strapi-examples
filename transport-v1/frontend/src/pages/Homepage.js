import React from 'react'
import { Link } from 'react-router-dom'
import { useQuery, gql } from '@apollo/client'
//import ReactMarkdown from 'react-markdown'

/*
const REVIEWS = gql`
    query GetReviews {
        rewiews {
            title,
            body,
            rating,
            id,
            categories {
                id,
                name
            }
        }
    }
`
*/

const REVIEWS = gql`
    query GetReviews {
        steps {
            id,
            Time,
            stop_transport{
              id,
              Name,
            }
        }
    }
`

export default function Homepage() {
    const { loading, error, data } = useQuery(REVIEWS)

    if (loading) return <p>Loading...</p>
    if (error) return <p>Error :(</p>

    console.log(data)

    return (
        <div>
            {data.steps.map(review => (
                <div key={review.id} className="review-card">
                    <p>review.id - {review.id}</p>
                    <p>review.Time - {review.Time.substring(0, 5)}</p>
                    <p>review.stop_transport.id - {review.stop_transport.id}</p>
                    <p>review.stop_transport.Name - {review.stop_transport.Name}</p>
                    <Link to={`/details/${review.id}`}>Read more</Link>
                </div>
            ))}
        </div>
    )
}

//<ReactMarkdown>{review.body.substring(0, 200)}</ReactMarkdown>