# strapi-examples

# strapi start

npx create-strapi-app backend
yarn create strapi-app backend

npm run develop
yarn develop

# react start

npx create-react-app frontend

yarn start
    Starts the development server.

yarn build
    Bundles the app into static files for production.

yarn test
    Starts the test runner.

yarn eject
    Removes this tool and copies build dependencies, configuration files
    and scripts into the app directory. If you do this, you can’t go back!

We suggest that you begin by typing:

  cd frontend
  yarn start

https://www.youtube.com/watch?v=qUiox_rkdvw&list=PL4cUxeGkcC9h6OY8_8Oq6JerWqsKdAPxn&index=10


# nextjs start

npx create-next-app --help
yarn create next-app --help

yarn create next-app next-01 --ts

  yarn dev
    Starts the development server.

  yarn build
    Builds the app for production.

  yarn start
    Runs the built app in production mode.

We suggest that you begin by typing:

  cd next-01
  yarn dev