import React from 'react'
import { useParams } from 'react-router-dom'
import { useQuery, gql } from '@apollo/client'
import ReactMarkdown from 'react-markdown'

const REVIEW = gql`
  query GetReview($id: ID!) {
    rewiew(id: $id) {
      title,
      body,
      rating,
      id,
      categories {
        name,
        id
      }
    }
  }
`

export default function ReviewDetails() {
    const { id } = useParams()
    const { loading, error, data } = useQuery(REVIEW, {
        variables: { id: id }
    })

    if (loading) return <p>Loading...</p>
    if (error) return <p>Error :(</p>

    console.log(data)

    return (
        <div className="review-card">
            <div className="rating">{data.rewiew.rating}</div>
            <h2>{data.rewiew.title}</h2>

            {data.rewiew.categories.map(c => (
                <small key={c.id}>{c.name}</small>
            ))}

            <ReactMarkdown>{data.rewiew.body}</ReactMarkdown>
            
        </div>
    )
}
